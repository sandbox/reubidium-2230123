<?php

/**
 * @file
 * Demote Filter
 */

/**
 * Implements hook_filter_info().
 */
function demote_filter_filter_info() {
  $filters['demote_filter_headings'] = array(
    'title' => t('Demote heading tags'),
    'description' => t('Demotes heading tags by one level.'),
    'settings callback' => '_demote_filter_headings_settings',
    'process callback' => '_demote_filter_headings_process',
    'tips callback' => '_demote_filter_headings_tips',
    'default settings' => array(
      'add_class' => '',
    ),
    'weight' => 10,
  );
  return $filters;
}

/**
 * Implements callback_filter_settings().
 *
 * Settings form for demote_filter.
 */
function _demote_filter_headings_settings($form, &$form_state, $filter, $format, $defaults, $filters) {
  $filter->settings += $defaults;

  $elements = array();
  $elements['add_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Add extra CSS class to elements.'),
    '#default_value' => $filter->settings['add_class'],
  );
  return $elements;
}

/**
 * Validation callback for demote_filter settings form.
 */
function _demote_filter_headings_settings_validate($form, &$form_state) {
  $form_state['values']['add_class'] = trim($form_state['values']['add_class']);
}

/**
 * Implements callback_filter_process().
 *
 * Demotes h1..h5 tags by one.
 */
function _demote_filter_headings_process($text, $filter) {
  $pattern = '/<h([1-5])( .*(?:=(?:[\'|\"]).*\3)?)?>(.*)<\/h\1>/isU';
  $replacement = '[demote_filter_$1$2]$3[/demote_filter_$1]';
  do {
    $text = preg_replace($pattern, $replacement, $text, -1, $count);
  } while ($count);

  $pattern = '/\[demote_filter_([1-5])(?: (.*(?:=(?:[\'|\"]).*\3)?))?\](.*)\[\/demote_filter_\1\]/isU';
  do {
    $text = preg_replace_callback($pattern, function($matches) use($filter) {
      return _demote_filter_headings_process_process($matches, $filter);
    }, $text, -1, $count);
  } while ($count);

  return $text;
}

/**
 * Callback function for preg_replace_callback().
 */
function _demote_filter_headings_process_process($matches, $filter) {
  $extra_parts = array();
  $level = ($matches[1] < 6) ? $matches[1] + 1 : 6;
  $attributes = explode(' ', $matches[2]);
  $contents = $matches[3];

  foreach ($attributes as $value) {
    if ($value) {
      list($key, $value) = array_pad(explode('=', $value), 2, '');
      $value = trim($value, '\'"');
      $extra_parts[$key] = $value;
    }
  }

  if (!empty($filter->settings['add_class'])) {
    $extra_parts['class'] = !empty($extra_parts['class']) ? $extra_parts['class'] . ' ' . $filter->settings['add_class'] : $filter->settings['add_class'];
  }

  $attributes = array();

  if ($extra_parts) {
    foreach ($extra_parts as $key => $value) {
      $attributes[] = empty($value) ? $key : $key . '=' . $value;
    }
  }

  return '<h' . $level . ' ' . implode(' ', $attributes) . '>' . $contents . '</h' . $level . '>';
}

/**
 * Implements callback_filter_tips().
 */
function _demote_filter_headings_tips() {
  return t('Demotes heading tags h1 thru h5 automatically.');
}
