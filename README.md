# Demote Filter

## Description:
As an input filter, "Demotes" heading tags by one.  This allows content editors to use WYSIWYG editors such as EpicEditor which create h1 tags without competing with the page's main h1 tag, solving an SEO annoyance.

## Installation:
Install as usual, enable for desired filters in admin/config/content/formats.

